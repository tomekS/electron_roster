var chokidar = require('chokidar');
var remote = require('remote');
var dialog = remote.require('dialog');

var fs = require('fs');
var employeesFile = null;
var requirementsFile = null;
var preferencesFile = null;
var workingDir = null;
var bestWatcher = null;
var population = null;
var cores = null;


document.getElementById('select-file-employee').addEventListener('click', function () {
    dialog.showOpenDialog(function (fileNames) {
        if (fileNames === undefined) {
            console.log("No file selected");
        } else {
            document.getElementById("actual-file-emp").value = fileNames[0];
            employeesFile = fileNames;
        }
    });
}, false);


document.getElementById('select-file-req').addEventListener('click', function () {
    dialog.showOpenDialog(function (fileNames) {
        if (fileNames === undefined) {
            console.log("No file selected");
        } else {
            document.getElementById("actual-file-req").value = fileNames[0];
            requirementsFile = fileNames;
        }
    });
}, false);

document.getElementById('select-file-pre').addEventListener('click', function () {
    dialog.showOpenDialog(function (fileNames) {
        if (fileNames === undefined) {
            console.log("No file selected");
        } else {
            document.getElementById("actual-file-pre").value = fileNames[0];
            preferencesFile = fileNames;
        }
    });
}, false);

document.getElementById('work-dir-act').addEventListener('click', function () {
    dialog.showOpenDialog({
        title: "Select a folder",
        properties: ["openDirectory"]
    }, (folderPaths) => {

        if (folderPaths === undefined) {
            console.log("No destination folder selected");
            return;
        } else {
            document.getElementById("work-dir").value = folderPaths[0];
            workingDir = folderPaths;
        }
    });
}, false);

document.getElementById('generate').addEventListener('click', function () {
    start_watcher();
    var cmd = require('node-cmd');
    console.log("elo");
    var command = "java -jar IndPro.jar ";
    if (employeesFile !== null) {
        command += employeesFile;
        command += " ";
    } else {
        alert("Please choose file with employees");
        return;
    }
    if (requirementsFile !== null) {
        command += requirementsFile;
        command += " ";
    } else {
        alert("Please choose file with requirements");
        return;
    }
    if (preferencesFile !== null) {
        command += preferencesFile;
        command += " ";
    } else {
        command += "C:\\Users\\electron\\Desktop\\javaPart\\empty_pref.csv "
    }

    if (workingDir !== null) {
        command += workingDir;
        command += " ";
    } else {
        alert("Please choose working directory.");
        return;
    }

    var cores = document.getElementById("cores").value;
    command += cores;
    command += " ";

    var population = document.getElementById("population").value;

    command += population;

    var fixedCommand = "java -jar IndPro.jar C:\\Users\\electron\\Desktop\\javaPart\\employees.csv C:\\Users\\electron\\Desktop\\javaPart\\req.csv C:\\Users\\electron\\Desktop\\javaPart\\empty_pref.csv C:\\Users\\electron\\Desktop\\testJavaPart 8 1000";
    cmd.get(command, function (msg) {
        console.log(msg);
    });

});

function add_best(path) {
    console.log("best " + path);
    var ul = document.getElementById("best_rosters");
    var li = document.createElement("li");
    var a = document.createElement("a");
    a.appendChild(document.createTextNode(path));
    a.addEventListener('click', function () {
        show_roster(path);
    });
    li.appendChild(a);
    ul.appendChild(li);
}

function show_roster(path) {
    const remote = require('electron').remote;
    const BrowserWindow = remote.BrowserWindow;

    var win = new BrowserWindow({width: 800, height: 600});
    win.loadURL('file://' + __dirname + '/rosterView.html?data=' + path);
    win.setMenu(null);
    win.webContents.openDevTools();
}

function handle_error(path) {
    var text = fs.readFileSync(path);
    alert("Error! : " + text);
}

function start_watcher() {
    bestWatcher = chokidar.watch(workingDir + "/best", (eventType, filename) => {
        console.log(eventType, filename);
    }).on('add', path => {
        add_best(path)
    });

    errorWatcher = chokidar.watch(workingDir + "/errors.txt").on('change', path => {
        handle_error(path)
    })
}

document.getElementById("test").addEventListener("click", function () {
    // const remote = require('electron').remote;
    // const BrowserWindow = remote.BrowserWindow;
    //
    // var win = new BrowserWindow({ width: 800, height: 600 });
    // win.loadURL('file://' + __dirname + '/rosterView.html');
    // win.setMenu(null);
    // win.loadFile();
    // win.reload;


    var jsonFile = require('jsonfile');
    var fileName = 'best_2.json';
    // var json = null;
    // const jthf = require("json-to-html-form");
    //
    // var fidale = jsonFile.readFile(fileName, function(err, jsonData) {
    //     if (err) throw err;
    //     const html = jthf.getForm(jsonData);
    //     console.log(html)
    //     fs.writeFile("res.html", html);
    // });
    //
    // console.log(json);

    jsonFile.readFile(fileName, function (err, jsonData) {
        if (err) throw err;
        // console.log(jsonData);
        json_to_html(jsonData);
    });
});

function json_to_html(dataJSON) {
    var du = dataJSON.shiftSet.map(s => shift_to_html(s));
    console.log(du);
}

function shift_to_html(shift) {
    var btn = document.createElement("BUTTON");        // Create a <button> element
    var t = document.createTextNode("CLICK ME");
    return btn;
}

document.getElementById('test2').addEventListener('click', function (event) {
    console.log(event.target.value);
    const remote = require('electron').remote;
    const BrowserWindow = remote.BrowserWindow;

    var win = new BrowserWindow({width: 800, height: 600});
    win.loadURL('file://' + __dirname + '/rosterView.html?data=best_2.json');
    win.setMenu(null);
    win.webContents.openDevTools();
});